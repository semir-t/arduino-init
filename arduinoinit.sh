#!/bin/bash

makefile()
{
  BOARD="default"
  PS3='Chose your board: '
  options=("uno" "mini" "mega" "mega2560" "atmega8" "atmega168" "atmega328" "pro" "pro5v" "pro328" "pro5v328" "bt" "bt328" "diecimila" "fio" "lilypad" "lilypad328")
  select opt in "${options[@]}"
  do
    case $opt in
      "uno") BOARD="uno"
        ;;
      "mini") BOARD="mini"
        ;;
      "mega") BOARD="mega" 
        ;;
      "mega2560") BOARD="mega2560"
        ;;
      "atmega8") BOARD="atmega8"
        ;;
      "atmega168") BOARD="atmega168"
        ;;
      "atmega328") BOARD="atmega328"
        ;;
      "pro") BOARD="pro"
        ;;
      "pro5v") BOARD="pro5v"
        ;;
      "pro328") BOARD="pro328"
        ;;
      "pro5v328") BOARD= "pro5v328"
        ;;
      "bt") BOARD="bt"
        ;;
      "bt328") BOARD="bt328"
        ;;
      "diecimila") BOARD="diecimila"
        ;;
      "fio") BOARD="fio"
        ;;
      "lilypad") BOARD="lilypad"
        ;;
      "lilypad328") BOARD="lilypad328"
        ;;
      *)
        echo "Error : Input is not valid"
        echo "Exiting..."
        return 1 
        ;;
    esac
    break
  done
  [ -e Makefile ] && rm Makefile
  read -p "Enter your MCU: " MCU
  read -p "Enter F_CPU: " F_CPU
  read -p "Enter ARDUINO_PORT: " ARDUINO_PORT
  echo "ARDUINO_DIR = /usr/share/arduino">>Makefile
  echo "BOARD_TAG = $BOARD">>Makefile
  echo "ARDUINO_PORT = $ARDUINO_PORT">>Makefile
  echo "NO_CORE = 1">>Makefile
  echo "AVRDUDE_ARD_PROGRAMMER = arduino">>Makefile
  echo "HEX_MAXIMUM_SIZE = 30720">>Makefile
  echo "AVRDUDE_ARD_BAUDRATE = 115200">>Makefile
  echo "#ISP_LOW_FUSE = 0xFF">>Makefile
  echo "#ISP_HIGH_FUSE = 0xDA">>Makefile
  echo "#ISP_EXT_FUSE = 0x05">>Makefile
  echo "#ISP_LOCK_FUSE_PRE = 0x3F">>Makefile
  echo "#ISP_LOCK_FUSE_POST = 0x0F">>Makefile
  echo "MCU = $MCU">>Makefile
  echo "F_CPU = $F_CPU">>Makefile
  echo "VARIANT = standard">>Makefile
  echo "ARDUINO_LIBS =">>Makefile
  echo "include /usr/share/arduino/Arduino.mk">>Makefile
}

initializeProject()
{
  read -p "Project name: " PROJECT_NAME
  mkdir $PROJECT_NAME
  cd $PROJECT_NAME
}

clear
initializeProject
makefile
